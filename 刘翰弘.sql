-- 查询在 SC 表存在成绩的学生信息
	select b.* ,a.CId ,a.score
	from sc a inner join student b
	on a.SId = b.SId
	where  score > 0;
-- 查询「李」姓老师的数量
	select count(*) from Teacher
	where Tname like '李%';
-- 查询学过「张三」老师授课的同学的信息
	select a.* ,b.Tname
	from student a ,Teacher b
	where Tname like '张三';
-- 按平均成绩从高到低显示所有学生的所有课程的成绩
	select a.SId ,a.Sname ,b.score ,c.Cname,avg(score)
	from Student a inner join SC b
	on a.SId = b.SId
	join course c
	on b.CID = c.CID
	group by SId,Cname
	order by avg(score) desc;
-- 查询每门课程被选修的学生数
	select CId ,count(*) from sc group by CId;
-- 查询出只选修两门课程的学生学号和姓名
	select a.SId 学号,a.Sname 姓名,count(CId)
	from Student a left join SC b
	on a.SId = b.SId
	group by Sname,a.SId
	having count(CId) = 2;
-- 查询男生、女生人数
	select Ssex ,count(*) from Student group by Ssex;
-- 查询名字中含有「风」字的学生信息
	select * from Student 
	where Sname like '%风%';
-- 查询同名学生名单，并统计同名人数
	select Sname ,count(Sname) from Student group by Sname having count(Sname) >= 2;
-- 查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
	select a.Sname 学生姓名 ,b.score 分数
	from Student a left join SC b
	on a.SId = b.SId
	left join Course c
	on b.CId = c.CId
	where Cname = '数学' and score < 60 ;
-- 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
	select a.Sname 学生姓名 ,b.score 分数 ,c.Cname 课程名称
	from Student a left join SC b
	on a.SId = b.SId
	left join Course c
	on b.CId = c.CId;
-- 查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
	select a.Sname 姓名,b.score 分数,c.Cname 课程名称
	from Student a left join sc b
	on a.SId = b.SId
	left join Course c
	on b.CId = C.CId
	where score >= 70;
-- 查询存在不及格的课程
	select a.score 分数,b.Cname 课程名称
	from SC a left join Course b
	on a.CId = b.CId
	where score < 60;
-- 查询课程编号为 01 且课程成绩在 80 分及以上的学生的学号和姓名
	select a.SId 学号,a.Sname
	from Student a left join SC 
	on a.SId = SC.SId
	where CId = 01 and score >=80;
-- 求每门课程的学生人数
	select CId,count(*) from SC group by CId;
-- 统计每门课程的学生选修人数
	select CId,count(*) from SC group by CId;
-- 查询各科成绩最高分、最低分和平均分
	select CId ,max(score),min(score),avg(score) from SC group by CId;
-- 查询「李」姓学生的各科成绩
	select a.Sname 姓名,b.score 分数 ,c.Cname 课程名称
	from Student a left join SC b
	on a.SId =  b.SId
	left join Course c
	on b.CId = c.CId
	where Sname like'李%';
-- 查询男生、女生语文成绩的平均分
	select Ssex ,avg(score) from SC,Student group by Ssex;
-- 统计「李四」老师教几个学生
	select d.Tname,count(Sname)
	from Student a left join SC b
	on a.SId = b.SId
	left join Course c
	on b.CId = c.CId
	left join Teacher d
	on c.TId = d.TId
	where Tname = '李四'
	group by Tname;