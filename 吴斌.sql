-- 查询在 SC 表存在成绩的学生信息
	select a.*,b.score from Student a inner join sc b on a.Sid = b.Sid;
-- 查询「李」姓老师的数量
	select count(*) from Teacher where Tname like '李%';
-- 查询学过「张三」老师授课的同学的信息
	select a.*,b.cid,c.tid 
	from student a inner join sc b 
	on a.sid = b.sid 
	inner join course c 
	on b.cid = c.cid 
	where tid = 02;
-- 按平均成绩从高到低显示所有学生的所有课程的成绩
	select a.sid,avg(score) 
	from student a 
	left join sc b on a.sid = b.sid 
	group by sid 
	order by avg(score) desc;
-- 查询每门课程被选修的学生数
	select cid,count(*) from sc group by cid;
-- 查询出只选修两门课程的学生学号和姓名
	select a.sname,a.sid,count(cid) 
	from student a 
	left join sc b on a.sid = b.sid 
	group by a.sid,a.sname 
	having count(cid) = 2;
-- 查询男生、女生人数
	select ssex,count(*) from student group by ssex;
-- 查询名字中含有「风」字的学生信息
	select * from student where sname like '%风%';
-- 查询同名学生名单，并统计同名人数
	select sname,count(sname) 
	from student 
	group by sname 
	having count(sname) > 1;
-- 查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
	select a.sname,b.score,c.cname 
	from student a inner join sc b on a.sid = b.sid 
	inner join course c on b.cid = c.cid 
	where cname = '数学' and score < 60;
-- 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
	select a.*,b.* from student a left join sc b on a.sid = b.sid;
-- 查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
	select a.sname,b.score,c.cname 
	from student a 
	inner join sc b on a.sid = b.sid 
	inner join course c on b.cid = c.cid 
	where score > 70;
-- 查询存在不及格的课程
	select a.score,b.cname 
	from sc a 
	inner join course b 
	on a.cid = b.cid 
	where score < 60;
-- 查询课程编号为 01 且课程成绩在 80 分及以上的学生的学号和姓名
	select a.sname,b.sid 
	from student a 
	inner join sc b 
	on a.sid = b.sid
	where cid = 1 and score >= 80;
-- 求每门课程的学生人数
	select cid,count(sid) from sc group by cid;
-- 统计每门课程的学生选修人数
	select cid,count(sid) from sc group by cid;
-- 查询各科成绩最高分、最低分和平均分
	select max(score),min(score),avg(score) from sc group by cid;
-- 查询「李」姓学生的各科成绩
	select a.sname,b.score,b.cid 
	from student a
	inner join sc b
	on a.sid = b.sid
	where sname like '李%';
-- 查询男生、女生语文成绩的平均分
	select ssex,avg(score) from student,sc where cid = 01 group by ssex;
-- 统计「李四」老师教几个学生
	select count(sid) from sc where cid = 02;