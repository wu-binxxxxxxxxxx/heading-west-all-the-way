-- 查询在 SC 表存在成绩的学生信息
	select DISTINCT Student.* from SC left join Student on Student.Sid = SC.Sid ;
-- 查询「李」姓老师的数量
	select count(*) from Teacher where Tname like '李_'; 
-- 查询学过「张三」老师授课的同学的信息
	select Student.* from Student,SC where Student.Sid = SC.Sid and SC.Cid = 01;
-- 按平均成绩从高到低显示所有学生的所有课程的成绩
	select S.SId 学号, S.Sname 姓名, avg(sc.score) 成绩 from Student S left join sc on S.SId = sc.SId group by S.SId, S.Sname order by avg(sc.score) desc;
-- 查询每门课程被选修的学生数
	select Cid,count(*) from SC group by Cid;
-- 查询出只选修两门课程的学生学号和姓名
	select Student.Sid,Student.Sname from Student,SC where Student.Sid = SC.Sid group by Student.Sid,Student.Sname having count(*) = 2;
-- 查询男生、女生人数 
	select Ssex,count(*) from Student group by Ssex;
-- 查询名字中含有「风」字的学生信息
	select * from Student where Sname like '%风%';
-- 查询同名学生名单，并统计同名人数
	select Sname,count(*) from Student group by Sname having count(*) > 1;
-- 查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
	select Student.Sname,SC.score from Student inner join SC on Student.Sid = SC.Sid inner join Course on SC.Cid = Course.cid where Course.Cname = '数学' and SC.score < 60;
-- 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
	select Student.Sname,SC.Cid,SC.score from Student left join SC on Student.Sid = SC.Sid;  
-- 查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
	select Student.Sname,Course.Cname,SC.score from Student inner join SC on Student.Sid = SC.Sid inner join Course on SC.Cid = Course.Cid where SC.score > 70;
-- 查询存在不及格的课程
	select Course.Cname from SC,Course where SC.Cid = Course.Cid and SC.score < 60;
-- 查询课程编号为 01 且课程成绩在 80 分及以上的学生的学号和姓名
	select Student.Sid,Student.Sname from Student,SC where Student.Sid = SC.Sid and SC.Cid = 01 and SC.score >= 80;
-- 求每门课程的学生人数
	select Course.Cname,count(*) from SC,Course where SC.Cid = Course.Cid group by Course.Cname;
-- 统计每门课程的学生选修人数
	select Course.Cname,count(*) from SC,Course where SC.Cid = Course.Cid group by Course.Cname;
-- 查询各科成绩最高分、最低分和平均分
	select Course.Cname,max(score),min(score),avg(score) from SC,Course where SC.Cid = Course.Cid group by Course.Cname;
-- 查询「李」姓学生的各科成绩
	select Student.Sname,Course.Cname,SC.score from Student inner join SC on Student.Sid = SC.Sid inner join Course on SC.Cid = Course.Cid where Student.Sname like '李%';
-- 查询男生、女生语文成绩的平均分
	select Student.Ssex,avg(score) from Student,SC where Student.Sid = SC.Sid group by Student.Ssex;
-- 统计「李四」老师教几个学生
	select count(*) from SC inner join Course on SC.Cid = Course.Cid inner join Teacher on Course.Tid = Teacher.Tid group by Teacher.Tname having Teacher.Tname = '李四';

