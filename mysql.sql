-- 查询在 SC 表存在成绩的学生信息
select Student.*,score
from SC inner join student
on SC.SId = Student.SId ;
-- 查询「李」姓老师的数量
select * from Teacher where Tname like '李%';
-- 查询学过「张三」老师授课的同学的信息
select student.*,tname,cname
from sc inner join student
on sc.sid = student.sid 
inner join course
on sc.cid = course.cid
inner join teacher
on course.tid = teacher.tid
where teacher.tname = '张三'; 
-- 按平均成绩从高到低显示所有学生的所有课程的成绩
select a.sid,a.sname,avg(score) 
from student a left join sc b
on a.sid = b.sid
group by a.sid,a.sname
order by avg(score) desc;
-- 查询每门课程被选修的学生数
select cid,count(*)
from sc
group by cid;
-- 查询出只选修两门课程的学生学号和姓名
select a.SId,a.Sname,count(CId)
from Student a left join SC b
on a.SId = b.SId
group by a.Sname,a.SId
having count(CId) = 2;
-- 查询男生、女生人数
select ssex,count(*)
from student
group by ssex ;
-- 查询名字中含有「风」字的学生信息
select * from student where sname like '%风%';
-- 查询同名学生名单，并统计同名人数
select sname,count(sname)
from student
group by sname
having not count(sname) = 1;
-- 查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
select course.cname,student.sname,sc.score
from sc inner join student
on sc.sid = student.sid 
inner join course
on sc.cid = course.cid
where course.cname = '数学' and score < 60;
-- 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select a.sid,a.sname,c.cname,b.score
from student a left join sc b
on a.sid = b.sid 
left join course c
on b.cid = c.cid;
-- 查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
select course.cname,student.sname,sc.score
from sc inner join student
on sc.sid = student.sid 
inner join course
on sc.cid = course.cid
where score > 70;
-- 查询存在不及格的课程 
select a.cid,a.cname
from course a left join sc b
on a.cid = b.cid 
where score < 60
group by cid,cname;
-- 查询课程编号为 01 且课程成绩在 80 分及以上的学生的学号和姓名
select c.cid,c.cname,b.sid,b.sname,a.score
from sc a inner join student b
on a.sid = b.sid 
inner join course c
on a.cid = c.cid
where a.cid = '01' and a.score >= 80;
-- 求每门课程的学生人数
select a.cname,count(b.cid)
from course a left join sc b
on a.cid = b.cid 
left join student c
on b.sid = c.sid
group by a.cname;
-- 统计每门课程的学生选修人数
select a.cname,count(b.cid)
from course a left join sc b
on a.cid = b.cid 
left join student c
on b.sid = c.sid
group by a.cname;
-- 查询各科成绩最高分、最低分和平均分
select a.cname,max(score),min(score),avg(score)
from course a left join sc b
on a.cid = b.cid 
group by a.cname;
-- 查询「李」姓学生的各科成绩
select sname,cname,score
from sc a left join student b
on a.sid = b.sid
left join course c
on a.cid = c.cid
where sname like '李%';
-- 查询男生、女生语文成绩的平均分
select ssex,avg(score)
from sc a inner join student b
on a.sid = b.sid 
inner join course c
on a.cid = c.cid
where a.cid = '01'
group by b.ssex; 
-- 统计「李四」老师教几个学生
select tname,count(a.tname)
from teacher a left join course b
on a.tid = b.tid 
left join sc c
on b.cid = c.cid
left join student d
on c.sid = d.sid
where a.tname = '李四' 
group by a.tname;